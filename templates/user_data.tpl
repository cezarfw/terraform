#!/bin/bash

sudo adduser contaazul 

sudo echo 'keOnyLN##olacontaazul##FxFN6iBl)apHc' | passwd --stdin contaazul

echo "contaazul ALL=(ALL)  NOPASSWD:ALL" >> /etc/sudoers

sudo sed -i '/PasswordAuthentication yes/s/^#//' /etc/ssh/sshd_config

sudo sed -i "s/PasswordAuthentication no/#PasswordAuthentication no/g" /etc/ssh/sshd_config

sudo service sshd restart

if egrep -i '(debian|ubuntu)' /etc/*-release >/dev/null
then
    echo "Baseado em Debian - APT" > /tmp/userdata_status
    sudo apt-get -y update && sudo apt-get -y install git python3 python3-pip vim
    sudo rm /usr/bin/python && sudo /bin/ln -s python3 /usr/bin/python
    sudo curl -fsSL https://get.docker.com -o get-docker.sh && sudo sh get-docker.sh
    sudo systemctl enable docker && sudo systemctl start docker
    ( cd /opt ; git clone https://cezarfw@bitbucket.org/cezarfw/app-python.git )
    docker build -t app-contaazul /opt/app-python/.
    docker run -p8081:8081 --name app-contaazul app-contaazul:latest ${bucket_name} ${arquivo_name}

elif egrep -i '(centos|redhat)' /etc/*-release >/dev/null
then
    echo "Baseado em RedHat" > /tmp/userdata_status
    sudo yum -y install git python3 python3-pip vim
    sudo rm /usr/bin/python && sudo /bin/ln -s python3 /usr/bin/python
    sudo curl -fsSL https://get.docker.com -o get-docker.sh && sudo sh get-docker.sh
    sudo systemctl enable docker && sudo systemctl start docker
    ( cd /opt ; git clone https://cezarfw@bitbucket.org/cezarfw/app-python.git )
    docker build -t app-contaazul /opt/app-python/.
    docker run -p8081:8081 --name app-contaazul app-contaazul:latest ${bucket_name} ${arquivo_name}
    

else
    echo "Distribuição não eh suportada" > /tmp/userdata_status
fi

echo "Success!" >> /tmp/userdata_status