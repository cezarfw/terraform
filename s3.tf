
resource "aws_s3_bucket" "app-contaazul" {
  bucket = var.bucket_name
  acl    = "private"

  tags = {
    Name   = "app-contaazul"
  }
}