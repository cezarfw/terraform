
data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}


module "ec2-instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "2.15.0"
  
  name                            = var.name
  ami                             = var.name_ami
  key_name                        = var.key_name
  instance_type                   = var.instance_type
  subnet_id                       = tolist(data.aws_subnet_ids.all.ids)[1]
  vpc_security_group_ids          = var.security_group_id

  user_data                       = "${data.template_file.user_data.rendered}"
  iam_instance_profile            = var.iam_instance_profile

}


data "template_file" "user_data" {
  template = "${file("templates/user_data.tpl")}"

  vars = {
    bucket_name = var.bucket_name
    arquivo_name = var.arquivo_name
  }
}