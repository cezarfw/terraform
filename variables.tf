
variable "aws_region" {
  default = "us-east-1"
}

variable "iam_instance_profile" {
  default = "ROLE-CONTAAZUL"
}

variable "name" {}

variable "name_ami" {
  default = "ami-0ac80df6eff0e70b5"
}

variable "key_name" {
  default = "app-contaazul"
}

variable "instance_type" {
  default = "t3.nano"
}

variable "bucket_name" {}
variable "arquivo_name" {}

variable "security_group_id" {
  default = [
    "sg-07947d19ef141c74e"
  ]
}