output "public_dns" {
  description = "List of public DNS names assigned to the instances"
  value       = module.ec2-instance.public_dns
}

output "tags" {
  description = "List of tags"
  value       = module.ec2-instance.tags
}

output "instance_id" {
  description = "EC2 instance ID"
  value       = module.ec2-instance.id
}
